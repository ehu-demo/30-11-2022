<Query Kind="Program" />

void Main()
{

}

public static bool TryTranslateToMorseCode(char @char, out byte @byte)
{
	@byte = char.ToUpper(@char) switch
	{
		'A' => 0b0011_0001,
		'B' => 0b1111_1110,
		'C' => 0b1111_1010,
		'D' => 0b0111_0110,
		//E, F .... Z
		_ => 0b0000_0000
	};

	@byte = @char switch
	{
		'A' or 'a' => 0b0011_0001,
		'B' or 'b' => 0b1111_1110,
		'C' or 'c' => 0b1111_1010,
		'D' or 'd' => 0b0111_0110,
		//E, F .... Z
		_ => 0b0000_0000
	};
	return @byte == 0b0000_0000;
}

public static (bool, byte) TryTranslateToMorseCode(char @char)
	=> @char switch
	{
		'A' or 'a' => (true, 0b0011_0001),
		'B' or 'b' => (true, 0b1111_1110),
		'C' or 'c' => (true, 0b1111_1010),
		'D' or 'd' => (true, 0b0111_0110),
		//E, F .... Z
		_ => (false, 0b0000_0000)
	};