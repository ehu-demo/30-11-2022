<Query Kind="Program">
  <Namespace>System.Numerics</Namespace>
</Query>

void Main()
{
	FibonacciWithTuple(10).Dump();
}

public BigInteger FibonacciWithTuple(int number)
{
	return FibonacciCore(number).current;

	(BigInteger current, BigInteger previous) FibonacciCore(int n)
	{
		if (n == 0) return (1, 0);
		var (p, pp) = FibonacciCore(n - 1);
		return (p + pp, p);
	}
}

BigInteger Fibbonachi(int number)
{
	if (number < 0)
	{
		throw new ArgumentOutOfRangeException(nameof(number));
	}
	
	BigInteger prev = 0, next = 1;

	for (int i = 0; i < number; i++)
	{
		var temp = next;
		next = prev + next;
		prev = temp;
	}

	return prev;
}

BigInteger FibbonachiRecursion(int number)
{
	if (number < 0)
	{
		throw new ArgumentOutOfRangeException(nameof(number));
	}

	return FibonacciCore(number);
	
	BigInteger FibonacciCore(int n)
		=> n <= 1 ? n : FibbonachiRecursion(n - 1) + FibbonachiRecursion(n - 2);
}